#!/bin/bash

# Copyright 2019 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


cpu_sig="$(iucode_tool --scan-system 2>&1 | sed -e 's/.*with signature //')"
cpu_flags="$(cat /sys/devices/system/cpu/cpu0/microcode/processor_flags)"
avail_line="$(iucode_tool -l -s "$cpu_sig,$cpu_flags" -tb /lib/firmware/intel-ucode 2>&1 | grep "sig[[:space:]]*$cpu_sig")"
avail="$(echo "$avail_line" | sed -e 's/.*rev[[:space:]]*//; s/[,[:space:]].*//')"

if [ -z "$avail" ]; then
  echo "UNKNOWN: did not find available ucode"
  exit 3
fi

current=$(awk '$1 == "microcode" {print $3; exit}' < /proc/cpuinfo)

if [ -z "$current" ]; then
  echo "UNKNOWN: did not learn current ucode"
  exit 3
fi

if [ "$(printf "%d" "$avail")" -gt "$(printf "%d" "$current")" ]; then
  echo "WARN: current ucode is $current while $avail is available"
  exit 1
else
  echo "OK: current ucode $current greater or equal to available $avail"
  exit 0
fi
