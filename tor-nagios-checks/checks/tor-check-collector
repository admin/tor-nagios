#!/usr/bin/python3
#
# Copyright 2014--2020 The Tor Project
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above
#   copyright notice, this list of conditions and the following disclaimer
#   in the documentation and/or other materials provided with the
#   distribution.
#
# * Neither the names of the copyright owners nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""Nagios plugin to check that the CollecTor service is running and returns
recent data."""

import sys
import json
import datetime
import urllib.error
from urllib.request import urlopen
import argparse

# Standard Nagios return codes
OK, WARNING, CRITICAL, UNKNOWN = range(4)

# Default CollecTor host, unless another host is specified on the command line
COLLECTOR_HOST = "collector.torproject.org"

# URL to download and verify
COLLECTOR_INDEX_URL = '/index/index.json'

# Directory paths in the returned index.json file and maximum ages in hours
COLLECTOR_PATHS = {
    "/archive/bridgedb-metrics/": 72,
    "/archive/bridge-descriptors/extra-infos/": 72,
    "/archive/bridge-descriptors/server-descriptors/": 72,
    "/archive/bridge-descriptors/statuses/": 72,
    "/archive/bridge-pool-assignments/": 72,
    "/archive/exit-lists/": 72,
    "/archive/onionperf/": 72,
    "/archive/relay-descriptors/": 30 * 24,
    "/archive/relay-descriptors/bandwidths/": 72,
    "/archive/relay-descriptors/consensuses/": 72,
    "/archive/relay-descriptors/extra-infos/": 72,
    "/archive/relay-descriptors/microdescs/": 72,
    "/archive/relay-descriptors/server-descriptors/": 72,
    "/archive/relay-descriptors/votes/": 72,
    "/archive/snowflakes/": 72,
    "/archive/webstats/": 72,
    "/recent/bridgedb-metrics/": 60,
    "/recent/bridge-descriptors/extra-infos/": 3,
    "/recent/bridge-descriptors/server-descriptors/": 3,
    "/recent/bridge-descriptors/statuses/": 3,
    "/recent/bridge-pool-assignments/": 30,
    "/recent/exit-lists/": 3,
    "/recent/onionperf/": 30,
    "/recent/relay-descriptors/bandwidths/": 3,
    "/recent/relay-descriptors/consensuses/": 3,
    "/recent/relay-descriptors/extra-infos/": 3,
    "/recent/relay-descriptors/microdescs/consensus-microdesc/": 3,
    "/recent/relay-descriptors/microdescs/micro/": 3,
    "/recent/relay-descriptors/server-descriptors/": 3,
    "/recent/relay-descriptors/votes/": 3,
    "/recent/snowflakes/": 30,
    "/recent/webstats/": 30
}

# ISO datetime pattern
ISO_DATETIME = "%Y-%m-%d %H:%M"

# Time in hours for considering a timestamp as critically outdated rather than
# just outdated
CRITICALLY_OUTDATED_HOURS = 3

def test_collector(use_https, host, ignore_missing):

    # Fetch and parse CollecTor's index.json file.
    index_json_url = "http%s://%s%s" %\
                     ("s" if use_https else "", host, COLLECTOR_INDEX_URL, )
    status, message, response_dict = obtain_index(index_json_url)
    if status is not OK:
        return status, message

    # Go through all relevant directory paths, extract maximum last-modified
    # timestamps, and check whether they're older than they should be.
    messages = {}
    utcnow = datetime.datetime.utcnow()
    for path, max_hours in COLLECTOR_PATHS.items():
        status, message, max_last_modified = extract_timestamp(response_dict,
                                                               path, ignore_missing)
        if max_last_modified:
            status, message = compare_timestamp(path, max_last_modified,
                                                utcnow, max_hours)
        if message:
            if status not in messages:
                messages[status] = []
            messages[status].append(message)

    # Format messages on highest severity as result of this test.
    if UNKNOWN in messages:
        return UNKNOWN, "Got a valid response from %s with at least one "\
                        "timestamp being missing or malformed: %s" %\
                        (index_json_url, ", ".join(messages[UNKNOWN]), )
    elif CRITICAL in messages:
        return CRITICAL, "Got a valid response from %s with at least one "\
                         "timestamp being critically outdated: %s" %\
                         (index_json_url, ", ".join(messages[CRITICAL]), )
    elif WARNING in messages:
        return WARNING, "Got a valid response from %s with at least one "\
                        "timestamp being outdated: %s" %\
                        (index_json_url, ", ".join(messages[WARNING]), )
    elif OK in messages:
        return OK, "Got a valid response from %s with recent timestamps: %s" %\
                   (index_json_url, ", ".join(messages[OK]), )
    else:
        return UNKNOWN, "Got a valid response from %s, but did not check a "\
                        "single timestamp." % (index_json_url, )


def obtain_index(index_json_url):
    """Fetch and parse CollecTor's index.json file."""

    # Fetch index.json document from CollecTor service.
    try:
        response = urlopen(index_json_url)
    except urllib.error.URLError as error:
        return UNKNOWN, "Error fetching %s: %s" %\
                        (index_json_url, error.reason, ), None
    response_str = response.read().decode('utf-8')

    # Parse JSON-formatted response.
    try:
        response_dict = json.loads(response_str)
    except ValueError as error:
        return UNKNOWN, "Error parsing JSON format: %s %s" %\
                        (error, response_str[:40], ), None
    if response_dict is None:
        return UNKNOWN, "Unable to obtain or parse response from %s." %\
                        (index_json_url, ), None
    else:
        return OK, None, response_dict


def extract_timestamp(response_dict, path, ignore_missing):
    """Extract the maximum last-modified time in the given directory path."""

    # Change to the directory denoted by the given path.
    current_directory = response_dict
    subpaths = path.strip("/").split("/")
    for subpath in subpaths:
        if "directories" in current_directory:
            subdirectory = None
            for directory in current_directory["directories"]:
                if directory["path"] == subpath:
                    subdirectory = directory
                    break
            if subdirectory is not None:
                current_directory = subdirectory
            elif ignore_missing:
                return UNKNOWN, None, None
            else:
                return UNKNOWN, "%s (directory not found)" % (path, ), None

    # Find the maximum last-modified time of all files found in this directory.
    if "files" not in current_directory:
        return UNKNOWN, "%s (directory empty)" % (path, ), None
    max_last_modified = None
    for file in subdirectory["files"]:
        if "last_modified" not in file:
            continue
        if max_last_modified is None or file["last_modified"] > max_last_modified:
            max_last_modified = file["last_modified"]
    if max_last_modified is None:
        return UNKNOWN, "%s (last-modified time(s) not found)" % (path, ), None
    else:
        return OK, None, max_last_modified


def compare_timestamp(path, max_last_modified, utcnow, max_hours):
    """Parse the given timestamp, compare it to the current system time, and
    return a status code and message depending on whether it's outdated or
    not."""

    # Parse timestamp.
    try:
        max_last_modified_ts = datetime.datetime.strptime(max_last_modified,
                                                          ISO_DATETIME)
    except ValueError:
        return UNKNOWN, "%s = %s (unable to parse)" %\
                        (path, max_last_modified, )

    # Compute the age in hours.
    age_in_hours = (utcnow - max_last_modified_ts).total_seconds() / 3600

    # Return whether it's outdated or not.
    if age_in_hours > max_hours + CRITICALLY_OUTDATED_HOURS:
        return CRITICAL, "%s = %s (%d >= %d + %d hours ago)" %\
                         (path, max_last_modified, age_in_hours, max_hours,
                         CRITICALLY_OUTDATED_HOURS)

    elif age_in_hours > max_hours:
        return WARNING, "%s = %s (%d >= %d hours ago)" %\
                        (path, max_last_modified, age_in_hours, max_hours)

    else:
        return OK, "%s = %s (%d < %d hours ago)" %\
                   (path, max_last_modified, age_in_hours, max_hours)


if __name__ == "__main__":
    status, message = None, None

    host = None
    use_https = False

    parser = argparse.ArgumentParser(description='Check that the CollecTor service is running and returns recent data.')
    parser.add_argument('-s', dest='use_https', action='store_true',
                        default=False, help='Connect via https:// rather than http://')
    parser.add_argument('-m', dest='ignore_missing', action='store_true',
                        default=False, help='Ignore missing timestamps')
    parser.add_argument('host', nargs='?', default='collector.torproject.org',
                        help='CollecTor host name (default: %(default)s)')
    args = parser.parse_args()

    try:
        status, message = test_collector(args.use_https, args.host, args.ignore_missing)
    except KeyboardInterrupt:
        status, message = UNKNOWN, "Caught Control-C..."
    except Exception as e:
        status = UNKNOWN
        message = repr(e)
    finally:
        if status == OK:
            print("OK: %s" % message)
        elif status == WARNING:
            print("WARNING: %s" % message)
        elif status == CRITICAL:
            print("CRITICAL: %s" % message)
        else:
            print("UNKNOWN: %s" % message)
            status = UNKNOWN
        sys.exit(status)


